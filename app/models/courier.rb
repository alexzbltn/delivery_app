class Courier < ApplicationRecord
  validates_presence_of :name
  validates :email, presence: true, uniqueness: true

  has_many :packages, :dependent => :destroy
end
