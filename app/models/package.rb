class Package < ApplicationRecord
  validates :tracking_number, presence: true, uniqueness: true

  belongs_to :courier
end
