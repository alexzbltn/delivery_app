class PackagesController < ApplicationController
  before_action :set_courier, only: :create

  def create
    package = @courier.packages.new(package_params)

    package.save

    redirect_to @courier
  end

  private

  def set_courier
    @courier = Courier.find(params[:courier_id])
  end

  def package_params
    params.require(:package).permit(:tracking_number, :delivery_status)
  end
end
