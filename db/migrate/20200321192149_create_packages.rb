class CreatePackages < ActiveRecord::Migration[5.2]
  def change
    create_table :packages do |t|
      t.string :tracking_number, null: false, index: true
      t.boolean :delivery_status, null: false, default: false
      t.references :courier, foreign_key: true

      t.timestamps
    end
  end
end
