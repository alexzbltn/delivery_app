FactoryBot.define do
  factory :courier do
    name  { FFaker::Name.first_name }
    email { FFaker::Internet.safe_email }
  end
end
