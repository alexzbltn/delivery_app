FactoryBot.define do
  factory :package do
    tracking_number { FFaker::String.from_regexp(/\w{10}/) }
    delivery_status { [true, false].sample }
    courier
  end
end
