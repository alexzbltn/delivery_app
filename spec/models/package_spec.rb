require 'rails_helper'

RSpec.describe Package, type: :model do
  it { is_expected.to validate_presence_of(:tracking_number) }
  it { is_expected.to belong_to(:courier) }

  context 'when tracking number already exists' do
    subject(:package) { create(:package) }

    it { is_expected.to validate_uniqueness_of(:tracking_number) }
  end
end
