RSpec.describe Courier, type: :model do
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:email) }
  it { is_expected.to have_many(:packages) }

  context 'when email already exists' do
    subject(:courier) { create(:courier) }

    it { is_expected.to validate_uniqueness_of(:email) }
  end

  context 'when deleting courier' do
    let(:courier) { create(:courier) }
    let(:package) { create(:package, courier: courier) }

    it 'deletes associated packages' do
      expect(package.courier_id).to eq courier.id

      courier.destroy
      expect(Package.all).to be_empty
    end
  end
end
