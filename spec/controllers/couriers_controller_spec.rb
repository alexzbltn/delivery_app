require 'rails_helper'

RSpec.describe CouriersController, type: :controller do
  describe 'GET #index' do
    let(:courier) { FactoryBot.create(:courier) }

    before { get :index }

    it 'populates an array of couriers' do
      expect(assigns(:couriers)).to eq [courier]
    end

    it 'renders the :index view' do
      expect(response).to render_template :index
    end
  end

  describe 'GET #show' do
    let(:courier) { FactoryBot.create(:courier) }

    before { get :show, params: { id: courier.id } }

    it 'assigns the requested contact to @courier' do
      expect(assigns(:courier)).to eq(courier)
    end

    it 'renders the #show view' do
      expect(response).to render_template :show
    end
  end
end
